function getAgeAndSigns(){
    let dateOfBirth = prompt('Ввведите дату своего рождения в формате дд.мм.гггг');
    dateOfBirth = dateOfBirth.replace(/^(\d{2}).(\d{2}).(\d{4})/,'$3-$2-$1');// регулярные выражения приводим компоненты даты к формату JS
    dateOfBirth = new Date(dateOfBirth);
    let nowDate = new Date();
    let age = Math.floor((nowDate - dateOfBirth)/1000/3600/24/365);
    
    let day = dateOfBirth.getDate();
    let month = dateOfBirth.getMonth() + 1;
    
    let zodiacSign;
    
    if ( day > 21 && month == 3 || day < 20 && month == 4) zodiacSign = 'Овен';
    else if ( day > 21 && month == 4 || day < 20 && month == 5 ) zodiacSign = 'Телец';
    else if (day > 21 && month == 5 || day < 21 && month == 6) zodiacSign = 'Близнецы';
    else if (day > 22 && month == 6 || day < 22 && month == 7) zodiacSign = 'Рак';
    else if (day > 23 && month == 7 || day < 23 && month == 8) zodiacSign = 'Лев';
    else if (day > 24 && month == 8 || day < 23 && month == 9) zodiacSign = 'Дева';
    else if (day > 24 && month == 9 || day < 23 && month == 10) zodiacSign = 'Весы';
    else if (day > 24 && month == 10 || day < 22 && month == 11) zodiacSign = 'Скорпион';
    else if (day > 23 && month == 11 || day < 21 && month == 12) zodiacSign = 'Стрелец';
    else if (day > 22 && month == 12 || day < 20 && month == 1) zodiacSign = 'Козерог';
    else if (day > 21 && month == 1 || day < 20 && month == 2) zodiacSign = 'Водолей';
    else if (day > 21 && month == 2 || day < 20 && month == 3) zodiacSign = 'Рыбы';
    console.log(zodiacSign);
    
    let year = dateOfBirth.getFullYear();
    let signOfYear;
    
    for (let i = 1900; i <= year; i+=12) {
        if(year == i) signOfYear = 'Крысы';
    };
    for (let i = 1901; i <= year; i+=12) {
        if(i == year) signOfYear = 'Быка';
    };
    for (let i = 1902; i <= year; i+=12) {
        if(i == year) signOfYear = 'Тигра';
    };
    for (let i = 1903; i <= year; i+=12) {
        if(i == year) signOfYear = 'Кролика';
    };
    for (let i = 1904; i <= year; i+=12) {
        if(i == year) signOfYear = 'Дракона';
    };
    for (let i = 1905; i <= year; i+=12) {
        if(i == year) signOfYear = 'Змеи';
    };
    for (let i = 1906; i <= year; i+=12) {
        if(i == year) signOfYear = 'Лошади';
    };
    for (let i = 1907; i <= year; i+=12) {
        if(i == year) signOfYear = 'Козы';
    };
    for (let i = 1908; i <= year; i+=12) {
        if(i == year) signOfYear = 'Обезьяны';
    };
    for (let i = 1909; i <= year; i+=12) {
        if(i == year) signOfYear = 'Петуха';
    };
    for (let i = 1910; i <= year; i+=12) {
        if(i == year) signOfYear = 'Собаки';
    };
    for (let i = 1911; i <= year; i+=12) {
        if(i == year) signOfYear = 'Свиньи';
    };
    
    return console.log(`Вам лет ${age}, Вы родились в год ${signOfYear}, по гороскому Вы ${zodiacSign}`);
    
    };
    
    getAgeAndSigns();